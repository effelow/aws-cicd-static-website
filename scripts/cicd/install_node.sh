#!/bin/bash
set -e

NODE_VERSION=$1

if [ -z "$NODE_VERSION" ]; then
    echo "Usage: $0 <node_version>"
    echo "Example: $0 20.x"
    exit 1
fi

# Download and install NodeSource repository
NODESOURCE_URL="https://rpm.nodesource.com/pub_$NODE_VERSION/nodistro/repo/nodesource-release-nodistro-1.noarch.rpm"
yum install "$NODESOURCE_URL" -y

# Install Node.js
yum install nodejs -y --setopt=nodesource-nodejs.module_hotfixes=1

# Installing Yarn package manager
curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo
rpm --import https://dl.yarnpkg.com/rpm/pubkey.gpg

echo "Node.js (version $NODE_VERSION) and Yarn have been installed."
