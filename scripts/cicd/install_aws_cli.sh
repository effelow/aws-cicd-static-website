#!/bin/bash
set -e

echo "--> Installing AWS CLI"

# Create the .aws directory
mkdir -p ~/.aws

# Download the AWS CLI
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"

# Unzip the installer
unzip -q awscliv2.zip

# Run the installer
./aws/install

echo "AWS CLI has been installed."
