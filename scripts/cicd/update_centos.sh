#!/bin/bash
set -e

# Update system packages
yum -y update

# Install required packages
yum -y install yarn tar gzip unzip which


echo "CentOS system has been updated and required packages have been installed."
