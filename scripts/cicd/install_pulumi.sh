#!/bin/bash
set -e

echo "--> Installing Pulumi"

# Download and install Pulumi
curl -fsSL https://get.pulumi.com | sh

echo "Pulumi has been installed."
